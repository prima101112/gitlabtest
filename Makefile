build:
	go build

generate: build
	./gitlabtest -gen=yes

topn: build
	./gitlabtest
	./gitlabtest -topn=1000