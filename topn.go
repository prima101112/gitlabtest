package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

// DEBUG for debug mode and more verbose output
const DEBUG = true

func debug(a ...interface{}) {
	if DEBUG {
		fmt.Print(a)
	}
}

// topn is function for seperate the big file intosmall pieces
func topN(n int, file string) {
	var topn []int
	f, err := os.Open(file)
	check(err)
	defer f.Close()
	reader := bufio.NewReader(f)
	a := []byte("\n")
	filesto := 0
	//loop untill EOF
	for {
		//readstring with delimiter /n base on how we create generated file and break when its EOF
		line, errFile := reader.ReadString(a[0])
		if errFile == io.EOF || line == "\n" {
			break
		}
		i64, err := strconv.ParseInt(line[0:len(line)-1], 10, 0)
		i := int(i64)
		if err != nil {
			break
		}
		ft := strconv.Itoa(filesto)
		// do 20000 number per file (tried 200000 per file with 1 million numbers its take longer)
		if len(topn) < 20000-1 {
			topn = append(topn, i)
		} else {
			topn = append(topn, i)
			// get 20000 get it, sort, and save to file
			sortAndsaveFile(ft, topn)
			topn = nil
			filesto++
		}
	}
}

// sort and save little file
func sortAndsaveFile(fileindex string, topn []int) {
	//apply buble sort
	// just read the please inform us if you cant response in 10 days so use buble
	topn = sortTopN(topn)
	fi, erro := os.Create(FILETMP + fileindex)
	w := bufio.NewWriter(fi)
	for _, v := range topn {
		w.WriteString(strconv.Itoa(v) + "\n")
	}
	check(erro)
	w.Flush()
}

// buuble sort
func sortTopN(topn []int) []int {
	for i := len(topn) - 1; i > 0; i-- {
		for j := 0; j < i; j++ {
			if topn[j] < topn[j+1] {
				topn[j], topn[j+1] = topn[j+1], topn[j]
			}
		}
	}
	return topn
}

// get Top N is used when little file is already generated
func getTopN(n int) {
	var topn []int
	var readers []*bufio.Reader

	// save the reader on array
	for i := 0; i < 50; i++ {
		is := strconv.Itoa(i)
		f, err := os.Open(FILETMP + is)
		check(err)
		defer f.Close()
		reader := bufio.NewReader(f)
		readers = append(readers, reader)
	}
	a := []byte("\n")
	//loop till reach N for top N
	for {
		//loop the reader
		for inx, rd := range readers {
			fmt.Print("work in redears ", inx, "\n")
			line, errFile := rd.ReadString(a[0])
			if errFile == io.EOF || line == "\n" {
				break
			}
			i64, err := strconv.ParseInt(line[0:len(line)-1], 10, 0)
			i := int(i64)
			if err != nil {
				break
			}
			//crucial function where is the number should be and insert it to the topN
			enterItoTopN(&topn, i, n)
			if len(topn) >= n {
				break
			}
		}
		if len(topn) >= n {
			break
		}
	}
	// write top N to oputput file
	writeTopNtoFile(&topn)
}

// this function is crucial its decide where is the number would be placed
func enterItoTopN(topn *[]int, number int, maxn int) {
	if DEBUG {
		debug("start with number: ", number, "\n")
	}
	if len(*topn) == 0 {
		*topn = append(*topn, number)
	} else {
		if len(*topn) < maxn {
			toindex := len(*topn)
			debug("to index : ", toindex, "\n")
			for in := len(*topn) - 1; in >= 0; in-- {
				debug("in : ", in, "\n")
				if number > (*topn)[in] {
					debug("index", in, "con ", number, ": > : ", (*topn)[in], "\n")
					continue
				} else if number == (*topn)[in] {
					debug("index", in, "con ", number, ": == : ", (*topn)[in], "\n")
					toindex = in + 1
					debug("to index == : ", toindex, "\n")
					break
				} else if number < (*topn)[in] {
					debug("index", in, "con ", number, ": < : ", (*topn)[in], "\n")
					toindex = in + 1
					debug("to index : ", toindex, "\n")
					break
				}
			}
			insertToIndex(topn, toindex, number)
		}
	}
}

// thsi function is inserting certain number to certain index
func insertToIndex(topn *[]int, toindex int, number int) {
	*topn = append(*topn, (*topn)[len(*topn)-1])
	for i := len(*topn) - 2; i > toindex; i-- {
		(*topn)[i] = (*topn)[i+1]
	}
	(*topn)[toindex] = number
}

// write top N to oputput file
func writeTopNtoFile(topn *[]int) {
	f, err := os.Create(FILENAME_TOPN)
	check(err)
	defer f.Close()
	w := bufio.NewWriter(f)
	for _, v := range *topn {
		_, err := w.WriteString(strconv.Itoa(v) + "\n")
		check(err)
	}
	w.Flush()
}
