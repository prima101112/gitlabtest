package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// FILENAME is just constant filename for generating  simulation file
const FILENAME = "data.log"

// FILETMP is just constant for temp file that seperate large file
const FILETMP = "dat"

// FILENAME_TOPN is output file
const FILENAME_TOPN = "output.log"

// check standard check error and just print the error
func check(e error) {
	if e != nil {
		fmt.Print(e)
	}
}

// createRandFile is just create file with individual number with specified lines
func createRandFile(lines int) {
	f, err := os.Create(FILENAME)
	check(err)
	defer f.Close()

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	w := bufio.NewWriter(f)
	for i := 0; i < lines; i++ {
		line := r1.Intn(100)
		_, err := w.WriteString(strconv.Itoa(line) + "\n")
		check(err)
	}
	w.Flush()
}

//main function and have certain flag
func main() {
	defer track(time.Now(), "main function")
	isgeneratePtr := flag.String("gen", "no", "want to generate file?")
	getN := flag.String("topn", "no", "get after tmp files ready")
	flag.Parse()

	if *isgeneratePtr == "yes" {
		//call function create random file
		createRandFile(1000000)
	} else if *getN != "" {
		//get top N number file separated required
		i, _ := strconv.Atoi(*getN)
		getTopN(i)
	} else {
		//get seperate file first and sort it
		topN(30, FILENAME)
	}

}

//track for tracking time
func track(t time.Time, name string) {
	elapsed := time.Since(t)
	log.Printf("%s took %s", name, elapsed)
}
